# README

 Project:  yarn_inventory_api

 Description:  The yarn_inventory_api provides a service to maintain yarn inventory.  


 Example Attributes:  
 ```
   brand: Noro 
    color: Otaru 
    weight: Sport / Fine 
    fiber: Wool (40%),Nylon (25%),Silk (25%),Mohair (10%) 
    crochet_hook: 4mm 
    needles: 3.25mm (3 US) (10 UK) 
    length: 300 
    length_unit: m 
    care: Hand wash 
    name: Silk Garden Sock Yarn 
    quantity: 2
```

 The documentation directory contains three methods to run the APIs:
 1. yarn_api.postman_collection.json -  this file can be imported into the Postman API tool
 2. ruby_examples.rb - this file contains snippets of ruby code to call the APIs 
 3. cURL_examples.sh - this file contains cURL commands to call the APIs 
note: these examples assume running locally, for example http://localhost:3000/yarns 
 
  * ruby 2.5.8p224 (2020-03-31 revision 67882) [x86_64-darwin16]

  * rails 6.0.3.2 
  	This application uses the --api option to create the project.  More information is available here: https://guides.rubyonrails.org/api_app.html 

  * SQLite version 3.16.0

