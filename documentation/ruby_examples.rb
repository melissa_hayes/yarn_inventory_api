# INDEX 
require 'uri'
require 'net/http'

url = URI("http://localhost:3000/yarns")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["cache-control"] = 'no-cache'
request["Postman-Token"] = 'a127186f-4338-46d9-9353-77ee51f32adc'

response = http.request(request)
puts response.read_body


# CREATE
require 'uri'
require 'net/http'

url = URI("http://localhost:3000/yarns")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["Content-Type"] = 'application/json'
request["cache-control"] = 'no-cache'
request["Postman-Token"] = 'eae063ff-1a81-4ba0-89c2-89949d934ddb'
request.body = "{\n    \"brand\": \"Noro\",\n    \"color\": \"Otaru\",\n    \"weight\": \"Sport / Fine\",\n    \"fiber\": \"Wool (40%),Nylon (25%),Silk (25%),Mohair (10%)\",\n    \"crochet_hook\": \"4mm\",\n    \"needles\": \"3.25mm (3 US) (10 UK)\",\n    \"length\": 300,\n    \"length_unit\": \"m\",\n    \"care\": \"Hand wash\",\n    \"name\": \"Silk Garden Sock Yarn\",\n    \"quantity\": 2\n}"

response = http.request(request)
puts response.read_body


# SHOW 
require 'uri'
require 'net/http'

url = URI("http://localhost:3000/yarns/1")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["cache-control"] = 'no-cache'
request["Postman-Token"] = '1aa4dc78-592f-4644-a550-cd44aafe8a0e'

response = http.request(request)
puts response.read_body



# DELETE
require 'uri'
require 'net/http'

url = URI("http://localhost:3000/yarns/1")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Delete.new(url)
request["cache-control"] = 'no-cache'
request["Postman-Token"] = 'db8d4b87-8fc4-4995-a048-b804891f980f'

response = http.request(request)
puts response.read_body


