# INDEX
curl -X GET \
  http://localhost:3000/yarns \
  -H 'Postman-Token: 8359d8c5-39e3-435d-9bb1-09e4b2690020' \
  -H 'cache-control: no-cache'


# CREATE   
curl -X POST \
  http://localhost:3000/yarns \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: ee752e2e-b8df-4457-aca3-51fa24a9c1fb' \
  -H 'cache-control: no-cache' \
  -d '{
    "brand": "Noro",
    "color": "Otaru",
    "weight": "Sport / Fine",
    "fiber": "Wool (40%),Nylon (25%),Silk (25%),Mohair (10%)",
    "crochet_hook": "4mm",
    "needles": "3.25mm (3 US) (10 UK)",
    "length": 300,
    "length_unit": "m",
    "care": "Hand wash",
    "name": "Silk Garden Sock Yarn",
    "quantity": 2
}'

# SHOW 
curl -X GET \
  http://localhost:3000/yarns/1 \
  -H 'Postman-Token: 69795119-23b9-4f4a-8667-00dee42f9cf1' \
  -H 'cache-control: no-cache'


# DELETE 
curl -X DELETE \
  http://localhost:3000/yarns/1 \
  -H 'Postman-Token: 0de082ce-4043-4691-b581-1434b1fe743c' \
  -H 'cache-control: no-cache'



