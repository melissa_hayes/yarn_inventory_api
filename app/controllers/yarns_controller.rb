class YarnsController < ApplicationController

  def index
    yarns = Yarn.all
    render json: yarns
  end

  def show
    yarn = Yarn.find(params[:id])
    render json: yarn
  end

  def create  
    Yarn.add_new_yarn(params)
  end

  def destroy
    Yarn.delete_row(params)
  end


end
