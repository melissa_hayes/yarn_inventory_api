class Yarn < ApplicationRecord

  
  validates :brand, presence: true


  def self.add_new_yarn(params)
 
    y = Yarn.new 
    y.brand = params[:brand]
    y.name = params[:name]
    y.color = params[:color]
    y.weight = params[:weight]    
    y.fiber = params[:fiber]
    y.crochet_hook = params[:crochet_hook]
    y.needles = params[:needles]
    y.length = params[:length]
    y.length_unit = params[:length_unit]
    y.quantity = params[:quantity]
    y.care = params[:care]
    y.save 

  end 

  def self.delete_row(params)
  	puts params[:id]
  	Yarn.destroy(params[:id])

  end 	
end
