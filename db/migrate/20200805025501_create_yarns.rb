class CreateYarns < ActiveRecord::Migration[6.0]
  def change
    create_table :yarns do |t|
      t.string :brand
      t.string :color
      t.string :weight
      t.string :fiber
      t.string :crochet_hook
      t.string :needles
      t.integer :length
      t.string :length_unit
      t.string :care

      t.timestamps
    end
  end
end
