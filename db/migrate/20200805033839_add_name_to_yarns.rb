class AddNameToYarns < ActiveRecord::Migration[6.0]
  def change
    add_column :yarns, :name, :string
  end
end
