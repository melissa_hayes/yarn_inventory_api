class AddQuantityToYarns < ActiveRecord::Migration[6.0]
  def change
    add_column :yarns, :quantity, :integer
  end
end
