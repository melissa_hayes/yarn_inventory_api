# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Yarn.delete_all
Yarn.create!(brand: "Furls", name:"Whims Merino",color: "Bold Intentions", weight: "Aran / Medium", fiber: "Merino Superwash Wool (50%)Nylon (50%)", crochet_hook: "4 - 5.5mm", needles: "4mm (6 US) (8 UK) 6mm (10 US) (4 UK)", length: 179 , length_unit: "m", quantity: 3, care: "Machine wash (30C), Dry flat")
Yarn.create!(brand: "Bernat", name:"Pop!",color: "Lipstick on Your Collar", weight: "Heavy Worsted", fiber: "Acrylic (100%)", crochet_hook: "5mm", needles: "5mm (8 US) (6 UK)", length: 280, length_unit: "yds",quantity: 12, care: "Machine wash (30C), Tumble dry (low)")
Yarn.create!(brand: "Bernat", name:"Pop!",color: "Ebony and Ivory", weight: "Heavy Worsted", fiber: "Acrylic (100%)", crochet_hook: "5mm", needles: "5mm (8 US) (6 UK)", length: 280, length_unit: "yds", quantity: 6, care: "Machine wash (30C), Tumble dry (low)")
Yarn.create!(brand: "Red Heart", name:"With Love Chunky",color: "lilac", weight: "Super Bulky / Super Chunky", fiber: "Acrylic (100%)", crochet_hook: "8mm", needles: "6.5mm (10.5 US) (3 UK)", length: 156, length_unit: "m", quantity: 3, care: "	Machine wash (40C), Tumble dry (low), Do not iron")

